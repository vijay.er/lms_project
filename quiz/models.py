from django.contrib.auth.models import User
from django.db import models
from django.db.models import Sum
from django.db.models.signals import post_save
from learningapp.models import DateTimeModel
from learningapp.models import Lesson


class Quiz(DateTimeModel):
    """ Quiz details """

    title = models.CharField(max_length=100, unique=True)
    lesson = models.ForeignKey(Lesson, on_delete=models.CASCADE, related_name='questions')
    no_of_questions = models.IntegerField(default=0)
    total_mark = models.IntegerField(default=0)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = 'Quiz'


class Question(DateTimeModel):
    """ Question details """

    question = models.CharField(max_length=200, unique=True)
    quiz = models.ForeignKey(Quiz, related_name='questions', on_delete=models.CASCADE)
    mark = models.IntegerField()
    question_choices = (
        ('MC', 'Multiple Choice'),
        ('FB', 'Fill In The Blank'),
        ('TF', 'True/False'),
        ('ET', 'Essay Type')
    )
    question_type = models.CharField(max_length=20,
                                     choices=question_choices,
                                     default=None)

    def __str__(self):
        return self.question

    class Meta:
        verbose_name_plural = 'Question'


def total_questions_marks(sender, instance, **kwargs):
    """ for average rating based on course"""
    num_of_ques = Question.objects.filter(quiz=instance.quiz).count()
    total_marks = Question.objects.filter(quiz=instance.quiz).aggregate(Sum("mark"))
    Quiz.objects.filter(title=instance.quiz).update(no_of_questions=num_of_ques, total_mark=total_marks['mark__sum'])


post_save.connect(total_questions_marks, sender=Question)


class QuestionMC(DateTimeModel):
    """for multiple choice questions"""
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    option1 = models.CharField(max_length=100)
    option2 = models.CharField(max_length=100)
    option3 = models.CharField(max_length=100)
    option4 = models.CharField(max_length=100)

    def __str__(self):
        return self.question.question

    class Meta:
        verbose_name_plural = 'QuestionMC'


class QuestionFB(DateTimeModel):
    """for fill-in-the-blank questions"""
    question = models.ForeignKey(Question, on_delete=models.CASCADE)

    def __str__(self):
        return self.question.question

    class Meta:
        verbose_name_plural = 'QuestionFB'


class QuestionTF(DateTimeModel):
    """for true/false questions"""
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    option1 = models.CharField(max_length=100)
    option2 = models.CharField(max_length=100)

    def __str__(self):
        return self.question.question

    class Meta:
        verbose_name_plural = 'QuestionTF'


class QuestionET(DateTimeModel):
    """for true/false questions"""
    question = models.ForeignKey(Question, on_delete=models.CASCADE)

    def __str__(self):
        return self.question.question

    class Meta:
        verbose_name_plural = 'QuestionET'


class QuizTakers(DateTimeModel):
    """ user details who attended the quiz"""

    user = models.ForeignKey(User, related_name='quiztakers', on_delete=models.CASCADE)
    quiz = models.ForeignKey(Quiz, related_name='quiztakers', on_delete=models.CASCADE)

    def __str__(self):
        return self.user.username


class Answer(DateTimeModel):
    """ Answer details """
    answer = models.CharField(max_length=200)
    answer_choices = (
        ('MT', 'MultiChoice/TrueFalse'),
        ('FE', 'FillBlank/EssayType')
    )
    answer_type = models.CharField(max_length=20,
                                   choices=answer_choices,
                                   default=None)
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='answers')
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    # is_correct = models.BooleanField(default=False)

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name_plural = 'Answer'


class AnswerEssayType(DateTimeModel):
    answer = models.ForeignKey(Answer, on_delete=models.CASCADE)

    def __str__(self):
        return self.answer


