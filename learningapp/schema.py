import graphql_jwt
from graphql_auth import mutations
from graphene_django.filter import DjangoFilterConnectionField
from learningapp.mutation import *
from graphql_jwt.decorators import login_required


class Mutation(object):
    register_user = mutations.Register.Field()
    token_auth = mutations.ObtainJSONWebToken.Field()  # for allow login with email or username
    # token_auth = graphql_jwt.ObtainJSONWebToken.Field() # for allow login with username only
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()
    course_enroll = EnrollCourse.Field()

    create_course = CreateCourse.Field()
    create_module = CreateModule.Field()
    create_lesson = CreateLesson.Field()
    create_rating = CreateRating.Field()
    update_course = UpdateCourse.Field()

    completed_lesson = CompletedLesson.Field()
    completed_module = CompletedModule.Field()
    completed_course = CompletedCourse.Field()

    create_quiz = CreateQuiz.Field()
    create_question = CreateQuestion.Field()

    create_answer = QuesAnswer.Field()


class Query(graphene.ObjectType):
    all_courses = DjangoFilterConnectionField(CourseNode)
    course = Node.Field(CourseNode)

    def resolve_all_courses(self, info):
        """ for all courses """
        user = info.context.user
        course = Course.objects.all()
        if user.is_anonymous:
            raise GraphQLError('Not logged in!')

        return course

    all_modules = DjangoFilterConnectionField(ModuleNode)
    module = Node.Field(ModuleNode)

    def resolve_all_modules(self, info):
        """ module details only available after login user"""

        if not info.context.user.is_authenticated:
            raise GraphQLError("you are not logged in")
        else:
            module = Module.objects.all()
            return module

    all_lesson = DjangoFilterConnectionField(LessonNode)
    lesson = Node.Field(LessonNode)

    def resolve_lesson(self, info):
        """ for lesson details """

        if not info.context.user.is_authenticated:
            raise GraphQLError("you are not logged in")
        else:
            return Lesson.objects.all()

    all_rating = DjangoFilterConnectionField(RatingNode)
    rating = graphene.Field(RatingNode)

    all_course_enrollment = DjangoFilterConnectionField(CourseEnrollNode)
    course_enrollment = graphene.Field(CourseEnrollNode)

    # all_complete_lesson = DjangoFilterConnectionField(CompleteLessonNode)
    # comp_lesson = graphene.Field(CompleteLessonNode)
    #
    # def resolve_comp_lesson(self, info):
    #     lesson = CompletedLessons.objects.filter(is_active=True)
    #     return lesson
