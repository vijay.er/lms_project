import graphene
from graphene_file_upload.scalars import Upload
from learningapp.datatype import *
from graphql import GraphQLError


class CreateCourse(graphene.Mutation):
    author = graphene.String()
    title = graphene.String()

    class Arguments:
        title = graphene.String()
        featured_image = Upload()
        author_designation = graphene.String()
        stats = graphene.String()
        description = graphene.String()
        language = graphene.String()
        price = graphene.Int()
        what_you_will_learn = graphene.String()
        requirements = graphene.String()

    def mutate(self, info, title, author_designation,
               stats, description, language, price, what_you_will_learn, requirements):
        img = info.context.FILES['featured_image']
        course = Course(title=title,
                        featured_image=img,
                        author=info.context.user,
                        author_designation=author_designation,
                        stats=stats,
                        description=description,
                        language=language,
                        price=price,
                        what_you_will_learn=what_you_will_learn,
                        requirements=requirements)
        course.save()
        return CreateCourse(author=course.author,
                            title=course.title)


class CreateModule(graphene.Mutation):
    course = graphene.Field(CourseNode)
    name = graphene.String()

    class Arguments:
        name = graphene.String()
        prerequisite = graphene.String()
        course_id = graphene.Int()

    def mutate(self, info, name, prerequisite, course_id):
        course_name = Course.objects.get(id=course_id)
        module = Module(name=name,
                        prerequisite=prerequisite,
                        course=course_name)
        module.save()
        return CreateModule(course=module.course,
                            name=module.name)


class CreateLesson(graphene.Mutation):
    module = graphene.Field(ModuleNode)
    lesson = graphene.String()

    class Arguments:
        name = graphene.String()
        featured_image = Upload()
        description = graphene.String()
        video = Upload()
        file = Upload()
        transcript = graphene.String()
        prerequisite = graphene.String()
        module_id = graphene.Int()

    def mutate(self, info, name, description, transcript, prerequisite, module_id):
        module_name = Module.objects.get(id=module_id)
        featured_img = info.context.FILES['featured_image']
        lesson_video = info.context.FILES['lesson_video']
        lesson_file = info.context.FILES['lesson_file']
        lesson = Lesson(name=name,
                        featured_image=featured_img,
                        description=description,
                        video=lesson_video,
                        file=lesson_file,
                        transcript=transcript,
                        prerequisite=prerequisite,
                        module=module_name)
        lesson.save()
        return CreateLesson(module=lesson.module,
                            lesson=lesson.name)


class CreateRating(graphene.Mutation):
    username = graphene.Field(UserTypeNode)
    course = graphene.Field(CourseNode)

    class Arguments:
        course_id = graphene.Int()
        rating = graphene.Int()

    def mutate(self, info, course_id, rating):
        course_name = Course.objects.get(id=course_id)
        username = info.context.user
        if course_name.author == username:
            raise GraphQLError("you can't rate your own course")
        if not 0 <= rating <= 5:
            raise GraphQLError("ratings must be 0 to 5")
        if not course_name.ratings.filter(user=username):
            rating = Rating(user=username,
                            rating=rating,
                            course=course_name)
            rating.save()
            return CreateRating(username=rating.user,
                                course=rating.course)
        raise GraphQLError("already rated the course")


class EnrollCourse(graphene.Mutation):
    username = graphene.String()
    course = graphene.Field(CourseNode)

    class Arguments:
        course_id = graphene.Int()
        message = graphene.String()

    def mutate(self, info, course_id):
        course_name = Course.objects.get(id=course_id)
        username = info.context.user
        if course_name.author == username:
            raise GraphQLError("you can't enroll your own course")
        if not course_name.course_enroll.filter(username=username):
            enroll = CourseEnroll(username=username,
                                  course=course_name)
            enroll.save()
            return CourseEnroll(username=enroll.username,
                                course=enroll.course)
        raise GraphQLError(message="already enrolled")


class UpdateCourse(graphene.Mutation):
    author = graphene.String()
    title = graphene.String()

    class Arguments:
        title = graphene.String()
        featured_image = Upload()
        author_designation = graphene.String()
        stats = graphene.String()
        description = graphene.String()
        language = graphene.String()
        price = graphene.Int()
        what_you_will_learn = graphene.String()
        requirements = graphene.String()

    def mutate(self, info, title, author_designation,
               stats, description, language, price, what_you_will_learn, requirements):
        img = info.context.FILES['featured_image']
        get_course = Course.objects.filter(author=info.context.user)
        if get_course:
            get_course.update(title=title,
                              featured_image=img,
                              author=info.context.user,
                              author_designation=author_designation,
                              stats=stats,
                              description=description,
                              language=language,
                              price=price,
                              what_you_will_learn=what_you_will_learn,
                              requirements=requirements)
            return UpdateCourse(author=info.context.user,
                                title=title)
        raise GraphQLError("you are not allowed to update this course")


class CompletedLesson(graphene.Mutation):
    username = graphene.String()
    lesson = graphene.Field(LessonNode)
    is_active = graphene.String()

    class Arguments:
        lesson_id = graphene.Int()
        is_completed = graphene.String()

    def mutate(self, info, lesson_id, is_completed):
        if not is_completed:
            raise GraphQLError("invalid data")
        if not is_completed == "True":
            raise GraphQLError("invalid data")
        lesson_name = Lesson.objects.get(id=lesson_id)
        username = info.context.user
        if not lesson_name.completed_lesson.filter(username=username):
            data = CompletedLessons(username=username,
                                    lesson=lesson_name,
                                    is_active=True)
            data.save()
            return CompletedLessons(username=data.username,
                                    lesson=data.lesson)

        raise GraphQLError("already completed")


class CompletedModule(graphene.Mutation):
    username = graphene.String()
    module = graphene.Field(ModuleNode)
    is_active = graphene.String()

    class Arguments:
        module_id = graphene.Int()
        is_completed = graphene.String()

    def mutate(self, info, module_id, is_completed):
        if not is_completed and not is_completed == "True":
            raise GraphQLError("invalid data")

        module_name = Module.objects.get(id=module_id)
        username = info.context.user
        if not module_name.completed_module.filter(username=username):
            data = CompletedModules(username=username,
                                    module=module_name,
                                    is_active=True)
            data.save()
            return CompletedModule(username=data.username,
                                   module=data.module)

        raise GraphQLError("already completed")


class CompletedCourse(graphene.Mutation):
    username = graphene.String()
    course = graphene.Field(CourseNode)
    is_active = graphene.String()

    class Arguments:
        course_id = graphene.Int()
        is_completed = graphene.String()

    def mutate(self, info, course_id, is_completed):
        if not is_completed and not is_completed == "True":
            raise GraphQLError("invalid data")

        course_name = Course.objects.get(id=course_id)
        username = info.context.user
        if not course_name.completed_course.filter(username=username):
            data = CompletedCourses(username=username,
                                    course=course_name,
                                    is_active=True)
            data.save()
            return CompletedCourse(username=data.username,
                                   course=data.course)

        raise GraphQLError("already completed")


class CreateQuiz(graphene.Mutation):
    title = graphene.String()
    lesson = graphene.Field(LessonNode)

    class Arguments:
        lesson_id = graphene.Int()
        title = graphene.String()

    def mutate(self, info, lesson_id, title):
        lesson_name = Lesson.objects.get(id=lesson_id)
        quiz = Quiz(title=title,
                    lesson=lesson_name)

        quiz.save()
        return CreateQuiz(title=quiz.title,
                          lesson=quiz.lesson)


class CreateQuestion(graphene.Mutation):
    quiz = graphene.Field(QuizNode)
    question = graphene.String()

    class Arguments:
        quiz_id = graphene.Int()
        question = graphene.String()
        mark = graphene.Int()
        question_type = graphene.String()
        option1 = graphene.String()
        option2 = graphene.String()
        option3 = graphene.String()
        option4 = graphene.String()

    def mutate(self, info, quiz_id, question, mark, question_type, **kwargs):
        quiz_title = Quiz.objects.get(id=quiz_id)
        question = Question(question=question,
                            quiz=quiz_title,
                            mark=mark,
                            question_type=question_type)
        question.save()
        ques = Question.objects.get(question=question)
        if question_type == "MC":
            option1 = kwargs.get('option1')
            option2 = kwargs.get('option2')
            option3 = kwargs.get('option3')
            option4 = kwargs.get('option4')

            mc_question = QuestionMC.objects.create(question=ques,
                                                    option1=option1,
                                                    option2=option2,
                                                    option3=option3,
                                                    option4=option4)

        elif question_type == "TF":
            option1 = kwargs.get('option1')
            option2 = kwargs.get('option2')
            tf_question = QuestionTF.objects.create(question=ques,
                                                    option1=option1,
                                                    option2=option2)
        elif question_type == "FB":
            fb_question = QuestionFB.objects.create(question=ques)

        elif question_type == "ET":
            et_question = QuestionET.objects.create(question=ques)

        return CreateQuestion(question=question.question,
                              quiz=question.quiz)


class QuesAnswer(graphene.Mutation):
    user = graphene.Field(UserTypeNode)
    message = graphene.String()

    class Arguments:
        question_id = graphene.Int()
        answer = graphene.String()
        is_correct = graphene.String()
        answer_type = graphene.String()

    def mutate(self, info, question_id, is_correct, answer_type, **kwargs):

        if not is_correct:
            raise GraphQLError("invalid answer")
        if not is_correct == "True":
            raise GraphQLError("invalid answer")

        question = Question.objects.get(id=question_id)
        username = info.context.user
        answer = kwargs.get('answer')

        data = Answer(user=username,
                      question=question,
                      answer=answer,
                      answer_type=answer_type
                        )
        data.save()
        return QuesAnswer(message='done')


