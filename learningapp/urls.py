from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path
from django.views.decorators.csrf import csrf_exempt
from graphene_django.views import GraphQLView

from learningapp.views import *

urlpatterns = [
          path('home/', index, name='home'),
          path('register/', user_register, name='register'),
          path('logout/', user_logout, name='logout'),
          path('courseenroll/', course_enroll, name='enroll'),
          path('login/', user_login, name='login'),
          path('changepswd/', change_password, name='password'),
          path('editcourse/', edit_course, name='editcourse'),
          path('module/<slug>/', module_view, name='module'),
          path('lesson/<slug>/', lesson_view, name='lesson'),
          path("", csrf_exempt(GraphQLView.as_view(graphiql=True))),

      ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
