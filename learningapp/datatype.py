from graphene_django.types import DjangoObjectType
from graphene import Node
from learningapp.models import *
from quiz.models import *


class UserTypeNode(DjangoObjectType):
    class Meta:
        model = User
        interfaces = (Node,)
        filter_fields = '__all__'


class CourseNode(DjangoObjectType):
    class Meta:
        model = Course
        filter_fields = ['title', 'author', 'author_designation', 'language',
                         'rating', 'price', 'what_you_will_learn', 'requirements']
        interfaces = (Node,)


class ModuleNode(DjangoObjectType):
    class Meta:
        model = Module
        filter_fields = ['name', 'duration', 'prerequisite', 'is_active']
        interfaces = (Node,)


class LessonNode(DjangoObjectType):
    class Meta:
        model = Lesson
        filter_fields = ['name']
        interfaces = (Node,)


class RatingNode(DjangoObjectType):
    class Meta:
        model = Rating
        filter_fields = ['course', 'rating', 'user']
        interfaces = (Node,)


class CourseEnrollNode(DjangoObjectType):
    class Meta:
        model = CourseEnroll
        filter_fields = ['username', 'course']
        interfaces = (Node,)


class CompletedLessonsNode(DjangoObjectType):
    class Meta:
        model = CompletedLessons
        interfaces = (Node,)
        filter_fields = '__all__'


class CompletedModulesNode(DjangoObjectType):
    class Meta:
        model = CompletedModules
        interfaces = (Node,)
        filter_fields = '__all__'


class CompletedCoursesNode(DjangoObjectType):
    class Meta:
        model = CompletedCourses
        interfaces = (Node,)
        filter_fields = '__all__'


class QuizNode(DjangoObjectType):
    class Meta:
        model = Quiz
        interfaces = (Node,)
        filter_fields = '__all__'


class QuestionNode(DjangoObjectType):
    class Meta:
        model = Question
        interfaces = (Node,)
        filter_fields = '__all__'


class MCQuestionNode(DjangoObjectType):
    class Meta:
        model = QuestionMC
        interfaces = (Node,)
        filter_fields = '__all__'


class TFQuestionNode(DjangoObjectType):
    class Meta:
        model = QuestionTF
        interfaces = (Node,)
        filter_fields = '__all__'


class FBQuestionNode(DjangoObjectType):
    class Meta:
        model = QuestionFB
        interfaces = (Node,)
        filter_fields = '__all__'


class ETQuestionNode(DjangoObjectType):
    class Meta:
        model = QuestionET
        interfaces = (Node,)
        filter_fields = '__all__'
