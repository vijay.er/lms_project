from django.contrib import admin
# from .forms import RegisterForm
from learningapp.models import *
from quiz.models import *

admin.site.register(Course)
admin.site.register(Module)
admin.site.register(Lesson)

admin.site.register(CourseEnroll)
admin.site.register(Rating)

admin.site.register(CompletedLessons)
admin.site.register(CompletedModules)
admin.site.register(CompletedCourses)


class QuizAdmin(admin.ModelAdmin):
    model = Quiz
    readonly_fields = ['no_of_questions', 'total_mark']


admin.site.register(Quiz, QuizAdmin)


class EssayTypeAnswerAdmin(admin.TabularInline):
    model = AnswerEssayType


class AnswerAdmin(admin.ModelAdmin):
    inlines = [EssayTypeAnswerAdmin]


admin.site.register(Answer, AnswerAdmin)
admin.site.register(QuizTakers)

admin.site.register(QuestionTF)
admin.site.register(QuestionFB)
admin.site.register(QuestionMC)
admin.site.register(QuestionET)


class FillInBlankAdmin(admin.TabularInline):
    model = QuestionFB


class MultiChoiceAdmin(admin.TabularInline):
    model = QuestionMC


class TrueFalseAdmin(admin.TabularInline):
    model = QuestionTF


class EssayAdmin(admin.TabularInline):
    model = QuestionET


class QuestionAdmin(admin.ModelAdmin):
    inlines = [MultiChoiceAdmin, TrueFalseAdmin, FillInBlankAdmin, EssayAdmin]


admin.site.register(Question, QuestionAdmin)
