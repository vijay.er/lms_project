import graphene
from graphene_django.debug import DjangoDebug
from graphene_file_upload.scalars import Upload
import learningapp.schema
from graphql_auth.schema import UserQuery, MeQuery


class Query(MeQuery, learningapp.schema.Query,):
    debug = graphene.Field(DjangoDebug, name="_debug")


class Mutation(learningapp.schema.Mutation, graphene.ObjectType,):
    pass


schema = graphene.Schema(query=Query, mutation=Mutation,types=[Upload])
